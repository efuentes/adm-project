lexer grammar VB6;

T10 : 'VERSION' ;
T11 : 'Begin' ;
T12 : 'VB.Form' ;
T13 : 'End' ;
T14 : 'VB.Frame' ;
T15 : 'VB.CommandButton' ;
T16 : 'VB.TextBox' ;

// $ANTLR src "c:\@dev\gra2mol\VB6\files\gen\VB6.ge3" 366
FloatingPointLiteral
    :   ('0'..'9')+ '.' ('0'..'9')*
    ;

// Lexer tokens
// $ANTLR src "c:\@dev\gra2mol\VB6\files\gen\VB6.ge3" 371
SLASH 			:	 '\u005C';
// $ANTLR src "c:\@dev\gra2mol\VB6\files\gen\VB6.ge3" 372
QUOTE			:	 '\u0027';
// $ANTLR src "c:\@dev\gra2mol\VB6\files\gen\VB6.ge3" 373
ID 				:	 ('a'..'z' | 'A'..'Z' | '_') ( 'a' .. 'z' | 'A' .. 'Z' | '0' .. '9' | '_' | '$' | '#' | SLASH | '-')*;
// $ANTLR src "c:\@dev\gra2mol\VB6\files\gen\VB6.ge3" 374
STRING  		:   '"' (~'"')* '"';
// $ANTLR src "c:\@dev\gra2mol\VB6\files\gen\VB6.ge3" 375
WS      		:   (' '|'\r'? '\n'|'\t')+ {skip();};