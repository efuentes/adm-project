// $ANTLR 3.0.1 c:\\@dev\\gra2mol\\VB6\\files\\gen\\VB6.ge3 2016-02-11 18:53:43

import org.antlr.runtime.*;
import java.util.Stack;
import java.util.List;
import java.util.ArrayList;

public class VB6Lexer extends Lexer {
    public static final int QUOTE=7;
    public static final int WS=9;
    public static final int SLASH=6;
    public static final int FloatingPointLiteral=4;
    public static final int T10=10;
    public static final int T11=11;
    public static final int T12=12;
    public static final int T13=13;
    public static final int T14=14;
    public static final int T15=15;
    public static final int ID=5;
    public static final int Tokens=17;
    public static final int T16=16;
    public static final int EOF=-1;
    public static final int STRING=8;
    public VB6Lexer() {;} 
    public VB6Lexer(CharStream input) {
        super(input);
    }
    public String getGrammarFileName() { return "c:\\@dev\\gra2mol\\VB6\\files\\gen\\VB6.ge3"; }

    // $ANTLR start T10
    public final void mT10() throws RecognitionException {
        try {
            int _type = T10;
            // c:\\@dev\\gra2mol\\VB6\\files\\gen\\VB6.ge3:3:5: ( 'VERSION' )
            // c:\\@dev\\gra2mol\\VB6\\files\\gen\\VB6.ge3:3:7: 'VERSION'
            {
            match("VERSION"); 


            }

            this.type = _type;
        }
        finally {
        }
    }
    // $ANTLR end T10

    // $ANTLR start T11
    public final void mT11() throws RecognitionException {
        try {
            int _type = T11;
            // c:\\@dev\\gra2mol\\VB6\\files\\gen\\VB6.ge3:4:5: ( 'Begin' )
            // c:\\@dev\\gra2mol\\VB6\\files\\gen\\VB6.ge3:4:7: 'Begin'
            {
            match("Begin"); 


            }

            this.type = _type;
        }
        finally {
        }
    }
    // $ANTLR end T11

    // $ANTLR start T12
    public final void mT12() throws RecognitionException {
        try {
            int _type = T12;
            // c:\\@dev\\gra2mol\\VB6\\files\\gen\\VB6.ge3:5:5: ( 'VB.Form' )
            // c:\\@dev\\gra2mol\\VB6\\files\\gen\\VB6.ge3:5:7: 'VB.Form'
            {
            match("VB.Form"); 


            }

            this.type = _type;
        }
        finally {
        }
    }
    // $ANTLR end T12

    // $ANTLR start T13
    public final void mT13() throws RecognitionException {
        try {
            int _type = T13;
            // c:\\@dev\\gra2mol\\VB6\\files\\gen\\VB6.ge3:6:5: ( 'End' )
            // c:\\@dev\\gra2mol\\VB6\\files\\gen\\VB6.ge3:6:7: 'End'
            {
            match("End"); 


            }

            this.type = _type;
        }
        finally {
        }
    }
    // $ANTLR end T13

    // $ANTLR start T14
    public final void mT14() throws RecognitionException {
        try {
            int _type = T14;
            // c:\\@dev\\gra2mol\\VB6\\files\\gen\\VB6.ge3:7:5: ( 'VB.Frame' )
            // c:\\@dev\\gra2mol\\VB6\\files\\gen\\VB6.ge3:7:7: 'VB.Frame'
            {
            match("VB.Frame"); 


            }

            this.type = _type;
        }
        finally {
        }
    }
    // $ANTLR end T14

    // $ANTLR start T15
    public final void mT15() throws RecognitionException {
        try {
            int _type = T15;
            // c:\\@dev\\gra2mol\\VB6\\files\\gen\\VB6.ge3:8:5: ( 'VB.CommandButton' )
            // c:\\@dev\\gra2mol\\VB6\\files\\gen\\VB6.ge3:8:7: 'VB.CommandButton'
            {
            match("VB.CommandButton"); 


            }

            this.type = _type;
        }
        finally {
        }
    }
    // $ANTLR end T15

    // $ANTLR start T16
    public final void mT16() throws RecognitionException {
        try {
            int _type = T16;
            // c:\\@dev\\gra2mol\\VB6\\files\\gen\\VB6.ge3:9:5: ( 'VB.TextBox' )
            // c:\\@dev\\gra2mol\\VB6\\files\\gen\\VB6.ge3:9:7: 'VB.TextBox'
            {
            match("VB.TextBox"); 


            }

            this.type = _type;
        }
        finally {
        }
    }
    // $ANTLR end T16

    // $ANTLR start FloatingPointLiteral
    public final void mFloatingPointLiteral() throws RecognitionException {
        try {
            int _type = FloatingPointLiteral;
            // c:\\@dev\\gra2mol\\VB6\\files\\gen\\VB6.ge3:367:5: ( ( '0' .. '9' )+ '.' ( '0' .. '9' )* )
            // c:\\@dev\\gra2mol\\VB6\\files\\gen\\VB6.ge3:367:9: ( '0' .. '9' )+ '.' ( '0' .. '9' )*
            {
            // c:\\@dev\\gra2mol\\VB6\\files\\gen\\VB6.ge3:367:9: ( '0' .. '9' )+
            int cnt1=0;
            loop1:
            do {
                int alt1=2;
                int LA1_0 = input.LA(1);

                if ( ((LA1_0>='0' && LA1_0<='9')) ) {
                    alt1=1;
                }


                switch (alt1) {
            	case 1 :
            	    // c:\\@dev\\gra2mol\\VB6\\files\\gen\\VB6.ge3:367:10: '0' .. '9'
            	    {
            	    matchRange('0','9'); 

            	    }
            	    break;

            	default :
            	    if ( cnt1 >= 1 ) break loop1;
                        EarlyExitException eee =
                            new EarlyExitException(1, input);
                        throw eee;
                }
                cnt1++;
            } while (true);

            match('.'); 
            // c:\\@dev\\gra2mol\\VB6\\files\\gen\\VB6.ge3:367:25: ( '0' .. '9' )*
            loop2:
            do {
                int alt2=2;
                int LA2_0 = input.LA(1);

                if ( ((LA2_0>='0' && LA2_0<='9')) ) {
                    alt2=1;
                }


                switch (alt2) {
            	case 1 :
            	    // c:\\@dev\\gra2mol\\VB6\\files\\gen\\VB6.ge3:367:26: '0' .. '9'
            	    {
            	    matchRange('0','9'); 

            	    }
            	    break;

            	default :
            	    break loop2;
                }
            } while (true);


            }

            this.type = _type;
        }
        finally {
        }
    }
    // $ANTLR end FloatingPointLiteral

    // $ANTLR start SLASH
    public final void mSLASH() throws RecognitionException {
        try {
            int _type = SLASH;
            // c:\\@dev\\gra2mol\\VB6\\files\\gen\\VB6.ge3:371:10: ( '\\u005C' )
            // c:\\@dev\\gra2mol\\VB6\\files\\gen\\VB6.ge3:371:13: '\\u005C'
            {
            match('\\'); 

            }

            this.type = _type;
        }
        finally {
        }
    }
    // $ANTLR end SLASH

    // $ANTLR start QUOTE
    public final void mQUOTE() throws RecognitionException {
        try {
            int _type = QUOTE;
            // c:\\@dev\\gra2mol\\VB6\\files\\gen\\VB6.ge3:372:9: ( '\\u0027' )
            // c:\\@dev\\gra2mol\\VB6\\files\\gen\\VB6.ge3:372:12: '\\u0027'
            {
            match('\''); 

            }

            this.type = _type;
        }
        finally {
        }
    }
    // $ANTLR end QUOTE

    // $ANTLR start ID
    public final void mID() throws RecognitionException {
        try {
            int _type = ID;
            // c:\\@dev\\gra2mol\\VB6\\files\\gen\\VB6.ge3:373:8: ( ( 'a' .. 'z' | 'A' .. 'Z' | '_' ) ( 'a' .. 'z' | 'A' .. 'Z' | '0' .. '9' | '_' | '$' | '#' | SLASH | '-' )* )
            // c:\\@dev\\gra2mol\\VB6\\files\\gen\\VB6.ge3:373:11: ( 'a' .. 'z' | 'A' .. 'Z' | '_' ) ( 'a' .. 'z' | 'A' .. 'Z' | '0' .. '9' | '_' | '$' | '#' | SLASH | '-' )*
            {
            if ( (input.LA(1)>='A' && input.LA(1)<='Z')||input.LA(1)=='_'||(input.LA(1)>='a' && input.LA(1)<='z') ) {
                input.consume();

            }
            else {
                MismatchedSetException mse =
                    new MismatchedSetException(null,input);
                recover(mse);    throw mse;
            }

            // c:\\@dev\\gra2mol\\VB6\\files\\gen\\VB6.ge3:373:39: ( 'a' .. 'z' | 'A' .. 'Z' | '0' .. '9' | '_' | '$' | '#' | SLASH | '-' )*
            loop3:
            do {
                int alt3=2;
                int LA3_0 = input.LA(1);

                if ( ((LA3_0>='#' && LA3_0<='$')||LA3_0=='-'||(LA3_0>='0' && LA3_0<='9')||(LA3_0>='A' && LA3_0<='Z')||LA3_0=='\\'||LA3_0=='_'||(LA3_0>='a' && LA3_0<='z')) ) {
                    alt3=1;
                }


                switch (alt3) {
            	case 1 :
            	    // c:\\@dev\\gra2mol\\VB6\\files\\gen\\VB6.ge3:
            	    {
            	    if ( (input.LA(1)>='#' && input.LA(1)<='$')||input.LA(1)=='-'||(input.LA(1)>='0' && input.LA(1)<='9')||(input.LA(1)>='A' && input.LA(1)<='Z')||input.LA(1)=='\\'||input.LA(1)=='_'||(input.LA(1)>='a' && input.LA(1)<='z') ) {
            	        input.consume();

            	    }
            	    else {
            	        MismatchedSetException mse =
            	            new MismatchedSetException(null,input);
            	        recover(mse);    throw mse;
            	    }


            	    }
            	    break;

            	default :
            	    break loop3;
                }
            } while (true);


            }

            this.type = _type;
        }
        finally {
        }
    }
    // $ANTLR end ID

    // $ANTLR start STRING
    public final void mSTRING() throws RecognitionException {
        try {
            int _type = STRING;
            // c:\\@dev\\gra2mol\\VB6\\files\\gen\\VB6.ge3:374:11: ( '\"' (~ '\"' )* '\"' )
            // c:\\@dev\\gra2mol\\VB6\\files\\gen\\VB6.ge3:374:15: '\"' (~ '\"' )* '\"'
            {
            match('\"'); 
            // c:\\@dev\\gra2mol\\VB6\\files\\gen\\VB6.ge3:374:19: (~ '\"' )*
            loop4:
            do {
                int alt4=2;
                int LA4_0 = input.LA(1);

                if ( ((LA4_0>='\u0000' && LA4_0<='!')||(LA4_0>='#' && LA4_0<='\uFFFE')) ) {
                    alt4=1;
                }


                switch (alt4) {
            	case 1 :
            	    // c:\\@dev\\gra2mol\\VB6\\files\\gen\\VB6.ge3:374:20: ~ '\"'
            	    {
            	    if ( (input.LA(1)>='\u0000' && input.LA(1)<='!')||(input.LA(1)>='#' && input.LA(1)<='\uFFFE') ) {
            	        input.consume();

            	    }
            	    else {
            	        MismatchedSetException mse =
            	            new MismatchedSetException(null,input);
            	        recover(mse);    throw mse;
            	    }


            	    }
            	    break;

            	default :
            	    break loop4;
                }
            } while (true);

            match('\"'); 

            }

            this.type = _type;
        }
        finally {
        }
    }
    // $ANTLR end STRING

    // $ANTLR start WS
    public final void mWS() throws RecognitionException {
        try {
            int _type = WS;
            // c:\\@dev\\gra2mol\\VB6\\files\\gen\\VB6.ge3:375:11: ( ( ' ' | ( '\\r' )? '\\n' | '\\t' )+ )
            // c:\\@dev\\gra2mol\\VB6\\files\\gen\\VB6.ge3:375:15: ( ' ' | ( '\\r' )? '\\n' | '\\t' )+
            {
            // c:\\@dev\\gra2mol\\VB6\\files\\gen\\VB6.ge3:375:15: ( ' ' | ( '\\r' )? '\\n' | '\\t' )+
            int cnt6=0;
            loop6:
            do {
                int alt6=4;
                switch ( input.LA(1) ) {
                case ' ':
                    {
                    alt6=1;
                    }
                    break;
                case '\n':
                case '\r':
                    {
                    alt6=2;
                    }
                    break;
                case '\t':
                    {
                    alt6=3;
                    }
                    break;

                }

                switch (alt6) {
            	case 1 :
            	    // c:\\@dev\\gra2mol\\VB6\\files\\gen\\VB6.ge3:375:16: ' '
            	    {
            	    match(' '); 

            	    }
            	    break;
            	case 2 :
            	    // c:\\@dev\\gra2mol\\VB6\\files\\gen\\VB6.ge3:375:20: ( '\\r' )? '\\n'
            	    {
            	    // c:\\@dev\\gra2mol\\VB6\\files\\gen\\VB6.ge3:375:20: ( '\\r' )?
            	    int alt5=2;
            	    int LA5_0 = input.LA(1);

            	    if ( (LA5_0=='\r') ) {
            	        alt5=1;
            	    }
            	    switch (alt5) {
            	        case 1 :
            	            // c:\\@dev\\gra2mol\\VB6\\files\\gen\\VB6.ge3:375:20: '\\r'
            	            {
            	            match('\r'); 

            	            }
            	            break;

            	    }

            	    match('\n'); 

            	    }
            	    break;
            	case 3 :
            	    // c:\\@dev\\gra2mol\\VB6\\files\\gen\\VB6.ge3:375:31: '\\t'
            	    {
            	    match('\t'); 

            	    }
            	    break;

            	default :
            	    if ( cnt6 >= 1 ) break loop6;
                        EarlyExitException eee =
                            new EarlyExitException(6, input);
                        throw eee;
                }
                cnt6++;
            } while (true);

            skip();

            }

            this.type = _type;
        }
        finally {
        }
    }
    // $ANTLR end WS

    public void mTokens() throws RecognitionException {
        // c:\\@dev\\gra2mol\\VB6\\files\\gen\\VB6.ge3:1:8: ( T10 | T11 | T12 | T13 | T14 | T15 | T16 | FloatingPointLiteral | SLASH | QUOTE | ID | STRING | WS )
        int alt7=13;
        switch ( input.LA(1) ) {
        case 'V':
            {
            switch ( input.LA(2) ) {
            case 'B':
                {
                int LA7_10 = input.LA(3);

                if ( (LA7_10=='.') ) {
                    switch ( input.LA(4) ) {
                    case 'T':
                        {
                        alt7=7;
                        }
                        break;
                    case 'F':
                        {
                        int LA7_19 = input.LA(5);

                        if ( (LA7_19=='r') ) {
                            alt7=5;
                        }
                        else if ( (LA7_19=='o') ) {
                            alt7=3;
                        }
                        else {
                            NoViableAltException nvae =
                                new NoViableAltException("1:1: Tokens : ( T10 | T11 | T12 | T13 | T14 | T15 | T16 | FloatingPointLiteral | SLASH | QUOTE | ID | STRING | WS );", 7, 19, input);

                            throw nvae;
                        }
                        }
                        break;
                    case 'C':
                        {
                        alt7=6;
                        }
                        break;
                    default:
                        NoViableAltException nvae =
                            new NoViableAltException("1:1: Tokens : ( T10 | T11 | T12 | T13 | T14 | T15 | T16 | FloatingPointLiteral | SLASH | QUOTE | ID | STRING | WS );", 7, 14, input);

                        throw nvae;
                    }

                }
                else {
                    alt7=11;}
                }
                break;
            case 'E':
                {
                int LA7_11 = input.LA(3);

                if ( (LA7_11=='R') ) {
                    int LA7_15 = input.LA(4);

                    if ( (LA7_15=='S') ) {
                        int LA7_21 = input.LA(5);

                        if ( (LA7_21=='I') ) {
                            int LA7_26 = input.LA(6);

                            if ( (LA7_26=='O') ) {
                                int LA7_28 = input.LA(7);

                                if ( (LA7_28=='N') ) {
                                    int LA7_30 = input.LA(8);

                                    if ( ((LA7_30>='#' && LA7_30<='$')||LA7_30=='-'||(LA7_30>='0' && LA7_30<='9')||(LA7_30>='A' && LA7_30<='Z')||LA7_30=='\\'||LA7_30=='_'||(LA7_30>='a' && LA7_30<='z')) ) {
                                        alt7=11;
                                    }
                                    else {
                                        alt7=1;}
                                }
                                else {
                                    alt7=11;}
                            }
                            else {
                                alt7=11;}
                        }
                        else {
                            alt7=11;}
                    }
                    else {
                        alt7=11;}
                }
                else {
                    alt7=11;}
                }
                break;
            default:
                alt7=11;}

            }
            break;
        case 'B':
            {
            int LA7_2 = input.LA(2);

            if ( (LA7_2=='e') ) {
                int LA7_12 = input.LA(3);

                if ( (LA7_12=='g') ) {
                    int LA7_16 = input.LA(4);

                    if ( (LA7_16=='i') ) {
                        int LA7_22 = input.LA(5);

                        if ( (LA7_22=='n') ) {
                            int LA7_27 = input.LA(6);

                            if ( ((LA7_27>='#' && LA7_27<='$')||LA7_27=='-'||(LA7_27>='0' && LA7_27<='9')||(LA7_27>='A' && LA7_27<='Z')||LA7_27=='\\'||LA7_27=='_'||(LA7_27>='a' && LA7_27<='z')) ) {
                                alt7=11;
                            }
                            else {
                                alt7=2;}
                        }
                        else {
                            alt7=11;}
                    }
                    else {
                        alt7=11;}
                }
                else {
                    alt7=11;}
            }
            else {
                alt7=11;}
            }
            break;
        case 'E':
            {
            int LA7_3 = input.LA(2);

            if ( (LA7_3=='n') ) {
                int LA7_13 = input.LA(3);

                if ( (LA7_13=='d') ) {
                    int LA7_17 = input.LA(4);

                    if ( ((LA7_17>='#' && LA7_17<='$')||LA7_17=='-'||(LA7_17>='0' && LA7_17<='9')||(LA7_17>='A' && LA7_17<='Z')||LA7_17=='\\'||LA7_17=='_'||(LA7_17>='a' && LA7_17<='z')) ) {
                        alt7=11;
                    }
                    else {
                        alt7=4;}
                }
                else {
                    alt7=11;}
            }
            else {
                alt7=11;}
            }
            break;
        case '0':
        case '1':
        case '2':
        case '3':
        case '4':
        case '5':
        case '6':
        case '7':
        case '8':
        case '9':
            {
            alt7=8;
            }
            break;
        case '\\':
            {
            alt7=9;
            }
            break;
        case '\'':
            {
            alt7=10;
            }
            break;
        case 'A':
        case 'C':
        case 'D':
        case 'F':
        case 'G':
        case 'H':
        case 'I':
        case 'J':
        case 'K':
        case 'L':
        case 'M':
        case 'N':
        case 'O':
        case 'P':
        case 'Q':
        case 'R':
        case 'S':
        case 'T':
        case 'U':
        case 'W':
        case 'X':
        case 'Y':
        case 'Z':
        case '_':
        case 'a':
        case 'b':
        case 'c':
        case 'd':
        case 'e':
        case 'f':
        case 'g':
        case 'h':
        case 'i':
        case 'j':
        case 'k':
        case 'l':
        case 'm':
        case 'n':
        case 'o':
        case 'p':
        case 'q':
        case 'r':
        case 's':
        case 't':
        case 'u':
        case 'v':
        case 'w':
        case 'x':
        case 'y':
        case 'z':
            {
            alt7=11;
            }
            break;
        case '\"':
            {
            alt7=12;
            }
            break;
        case '\t':
        case '\n':
        case '\r':
        case ' ':
            {
            alt7=13;
            }
            break;
        default:
            NoViableAltException nvae =
                new NoViableAltException("1:1: Tokens : ( T10 | T11 | T12 | T13 | T14 | T15 | T16 | FloatingPointLiteral | SLASH | QUOTE | ID | STRING | WS );", 7, 0, input);

            throw nvae;
        }

        switch (alt7) {
            case 1 :
                // c:\\@dev\\gra2mol\\VB6\\files\\gen\\VB6.ge3:1:10: T10
                {
                mT10(); 

                }
                break;
            case 2 :
                // c:\\@dev\\gra2mol\\VB6\\files\\gen\\VB6.ge3:1:14: T11
                {
                mT11(); 

                }
                break;
            case 3 :
                // c:\\@dev\\gra2mol\\VB6\\files\\gen\\VB6.ge3:1:18: T12
                {
                mT12(); 

                }
                break;
            case 4 :
                // c:\\@dev\\gra2mol\\VB6\\files\\gen\\VB6.ge3:1:22: T13
                {
                mT13(); 

                }
                break;
            case 5 :
                // c:\\@dev\\gra2mol\\VB6\\files\\gen\\VB6.ge3:1:26: T14
                {
                mT14(); 

                }
                break;
            case 6 :
                // c:\\@dev\\gra2mol\\VB6\\files\\gen\\VB6.ge3:1:30: T15
                {
                mT15(); 

                }
                break;
            case 7 :
                // c:\\@dev\\gra2mol\\VB6\\files\\gen\\VB6.ge3:1:34: T16
                {
                mT16(); 

                }
                break;
            case 8 :
                // c:\\@dev\\gra2mol\\VB6\\files\\gen\\VB6.ge3:1:38: FloatingPointLiteral
                {
                mFloatingPointLiteral(); 

                }
                break;
            case 9 :
                // c:\\@dev\\gra2mol\\VB6\\files\\gen\\VB6.ge3:1:59: SLASH
                {
                mSLASH(); 

                }
                break;
            case 10 :
                // c:\\@dev\\gra2mol\\VB6\\files\\gen\\VB6.ge3:1:65: QUOTE
                {
                mQUOTE(); 

                }
                break;
            case 11 :
                // c:\\@dev\\gra2mol\\VB6\\files\\gen\\VB6.ge3:1:71: ID
                {
                mID(); 

                }
                break;
            case 12 :
                // c:\\@dev\\gra2mol\\VB6\\files\\gen\\VB6.ge3:1:74: STRING
                {
                mSTRING(); 

                }
                break;
            case 13 :
                // c:\\@dev\\gra2mol\\VB6\\files\\gen\\VB6.ge3:1:81: WS
                {
                mWS(); 

                }
                break;

        }

    }


 

}