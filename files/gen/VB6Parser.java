// $ANTLR 3.0.1 c:\\@dev\\gra2mol\\VB6\\files\\gen\\VB6.ge3 2016-02-11 18:53:42

	import gts.modernization.model.CST.impl.*;
	import gts.modernization.model.CST.*;
	import java.util.Iterator;


import org.antlr.runtime.*;
import java.util.Stack;
import java.util.List;
import java.util.ArrayList;
import java.util.Map;
import java.util.HashMap;
import org.antlr.stringtemplate.*;
import org.antlr.stringtemplate.language.*;
import java.util.HashMap;
public class VB6Parser extends Parser {
    public static final String[] tokenNames = new String[] {
        "<invalid>", "<EOR>", "<DOWN>", "<UP>", "FloatingPointLiteral", "ID", "SLASH", "QUOTE", "STRING", "WS", "'VERSION'", "'Begin'", "'VB.Form'", "'End'", "'VB.Frame'", "'VB.CommandButton'", "'VB.TextBox'"
    };
    public static final int QUOTE=7;
    public static final int WS=9;
    public static final int SLASH=6;
    public static final int FloatingPointLiteral=4;
    public static final int ID=5;
    public static final int EOF=-1;
    public static final int STRING=8;

        public VB6Parser(TokenStream input) {
            super(input);
            ruleMemo = new HashMap[12+1];
         }
        
    protected StringTemplateGroup templateLib =
      new StringTemplateGroup("VB6ParserTemplates", AngleBracketTemplateLexer.class);

    public void setTemplateLib(StringTemplateGroup templateLib) {
      this.templateLib = templateLib;
    }
    public StringTemplateGroup getTemplateLib() {
      return templateLib;
    }
    /** allows convenient multi-value initialization:
     *  "new STAttrMap().put(...).put(...)"
     */
    public static class STAttrMap extends HashMap {
      public STAttrMap put(String attrName, Object value) {
        super.put(attrName, value);
        return this;
      }
      public STAttrMap put(String attrName, int value) {
        super.put(attrName, new Integer(value));
        return this;
      }
    }

    public String[] getTokenNames() { return tokenNames; }
    public String getGrammarFileName() { return "c:\\@dev\\gra2mol\\VB6\\files\\gen\\VB6.ge3"; }


    public static class module_return extends ParserRuleReturnScope {
        public Node returnNode;
        public StringTemplate st;
        public Object getTemplate() { return st; }
        public String toString() { return st==null?null:st.toString(); }
    };

    // $ANTLR start module
    // c:\\@dev\\gra2mol\\VB6\\files\\gen\\VB6.ge3:19:1: module returns [Node returnNode] : moduleHeaderGen+= moduleHeader moduleBodyGen+= moduleBody ;
    public final module_return module() throws RecognitionException {
        module_return retval = new module_return();
        retval.start = input.LT(1);
        int module_StartIndex = input.index();
        List list_moduleHeaderGen=null;
        List list_moduleBodyGen=null;
        RuleReturnScope moduleHeaderGen = null;
        RuleReturnScope moduleBodyGen = null;
        try {
            if ( backtracking>0 && alreadyParsedRule(input, 1) ) { return retval; }
            // c:\\@dev\\gra2mol\\VB6\\files\\gen\\VB6.ge3:20:1: (moduleHeaderGen+= moduleHeader moduleBodyGen+= moduleBody )
            // c:\\@dev\\gra2mol\\VB6\\files\\gen\\VB6.ge3:20:3: moduleHeaderGen+= moduleHeader moduleBodyGen+= moduleBody
            {
            pushFollow(FOLLOW_moduleHeader_in_module50);
            moduleHeaderGen=moduleHeader();
            _fsp--;
            if (failed) return retval;
            if (list_moduleHeaderGen==null) list_moduleHeaderGen=new ArrayList();
            list_moduleHeaderGen.add(moduleHeaderGen);

            pushFollow(FOLLOW_moduleBody_in_module54);
            moduleBodyGen=moduleBody();
            _fsp--;
            if (failed) return retval;
            if (list_moduleBodyGen==null) list_moduleBodyGen=new ArrayList();
            list_moduleBodyGen.add(moduleBodyGen);

            if ( backtracking==0 ) {
              
              		// Create return CST Node
              		Node moduleReturnNode = CSTFactoryImpl.eINSTANCE.createNode();
              		moduleReturnNode.setKind("module");
              	    // Create a CST Node
              		if(list_moduleHeaderGen != null) {
              	        for(Iterator it = list_moduleHeaderGen.iterator(); it.hasNext(); )  { 
              	            VB6Parser.moduleHeader_return r = (VB6Parser.moduleHeader_return) it.next(); 
              	            if(r != null && r.returnNode != null) {
              	            	r.returnNode.setKind("moduleHeader");
              	            	moduleReturnNode.getChildren().add(r.returnNode);
              	            } 
              	        }
              	    }
              	    // Create a CST Node
              		if(list_moduleBodyGen != null) {
              	        for(Iterator it = list_moduleBodyGen.iterator(); it.hasNext(); )  { 
              	            VB6Parser.moduleBody_return r = (VB6Parser.moduleBody_return) it.next(); 
              	            if(r != null && r.returnNode != null) {
              	            	r.returnNode.setKind("moduleBody");
              	            	moduleReturnNode.getChildren().add(r.returnNode);
              	            } 
              	        }
              	    }
              		// Returns the Node with CST Leaves/Nodes
              		retval.returnNode = moduleReturnNode;
              	
            }

            }

            retval.stop = input.LT(-1);

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
            if ( backtracking>0 ) { memoize(input, 1, module_StartIndex); }
        }
        return retval;
    }
    // $ANTLR end module

    public static class moduleHeader_return extends ParserRuleReturnScope {
        public Node returnNode;
        public StringTemplate st;
        public Object getTemplate() { return st; }
        public String toString() { return st==null?null:st.toString(); }
    };

    // $ANTLR start moduleHeader
    // c:\\@dev\\gra2mol\\VB6\\files\\gen\\VB6.ge3:50:1: moduleHeader returns [Node returnNode] : TK_0= 'VERSION' FloatingPointLiteralGen= FloatingPointLiteral ;
    public final moduleHeader_return moduleHeader() throws RecognitionException {
        moduleHeader_return retval = new moduleHeader_return();
        retval.start = input.LT(1);
        int moduleHeader_StartIndex = input.index();
        Token TK_0=null;
        Token FloatingPointLiteralGen=null;

        try {
            if ( backtracking>0 && alreadyParsedRule(input, 2) ) { return retval; }
            // c:\\@dev\\gra2mol\\VB6\\files\\gen\\VB6.ge3:51:1: (TK_0= 'VERSION' FloatingPointLiteralGen= FloatingPointLiteral )
            // c:\\@dev\\gra2mol\\VB6\\files\\gen\\VB6.ge3:51:3: TK_0= 'VERSION' FloatingPointLiteralGen= FloatingPointLiteral
            {
            TK_0=(Token)input.LT(1);
            match(input,10,FOLLOW_10_in_moduleHeader75); if (failed) return retval;
            FloatingPointLiteralGen=(Token)input.LT(1);
            match(input,FloatingPointLiteral,FOLLOW_FloatingPointLiteral_in_moduleHeader79); if (failed) return retval;
            if ( backtracking==0 ) {
              
              		// Create return CST Node
              		Node moduleHeaderReturnNode = CSTFactoryImpl.eINSTANCE.createNode();
              		moduleHeaderReturnNode.setKind("moduleHeader");
              	    // Create a Token CST Leaf	
              	    if(TK_0 != null) {
              			Leaf TK_0Leaf = CSTFactoryImpl.eINSTANCE.createLeaf();
              	 		TK_0Leaf.setKind("TOKEN");
              	 		TK_0Leaf.setValue(TK_0.getText());
              			TK_0Leaf.setPos(TK_0.getCharPositionInLine());
              			TK_0Leaf.setLine(TK_0.getLine());
              	 		moduleHeaderReturnNode.getChildren().add(TK_0Leaf);
              	 	}
              	    // Create a CST Leaf
              		if(FloatingPointLiteralGen != null) {
              			Leaf FloatingPointLiteralGenLeaf = CSTFactoryImpl.eINSTANCE.createLeaf();
              			FloatingPointLiteralGenLeaf.setKind("FloatingPointLiteral");
              			FloatingPointLiteralGenLeaf.setValue(FloatingPointLiteralGen.getText());
              			FloatingPointLiteralGenLeaf.setPos(FloatingPointLiteralGen.getCharPositionInLine());
              			FloatingPointLiteralGenLeaf.setLine(FloatingPointLiteralGen.getLine());
              			moduleHeaderReturnNode.getChildren().add(FloatingPointLiteralGenLeaf);
              		}
              		// Returns the Node with CST Leaves/Nodes
              		retval.returnNode = moduleHeaderReturnNode;
              	
            }

            }

            retval.stop = input.LT(-1);

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
            if ( backtracking>0 ) { memoize(input, 2, moduleHeader_StartIndex); }
        }
        return retval;
    }
    // $ANTLR end moduleHeader

    public static class moduleBody_return extends ParserRuleReturnScope {
        public Node returnNode;
        public StringTemplate st;
        public Object getTemplate() { return st; }
        public String toString() { return st==null?null:st.toString(); }
    };

    // $ANTLR start moduleBody
    // c:\\@dev\\gra2mol\\VB6\\files\\gen\\VB6.ge3:79:1: moduleBody returns [Node returnNode] : vbFormDefinitionGen+= vbFormDefinition ;
    public final moduleBody_return moduleBody() throws RecognitionException {
        moduleBody_return retval = new moduleBody_return();
        retval.start = input.LT(1);
        int moduleBody_StartIndex = input.index();
        List list_vbFormDefinitionGen=null;
        RuleReturnScope vbFormDefinitionGen = null;
        try {
            if ( backtracking>0 && alreadyParsedRule(input, 3) ) { return retval; }
            // c:\\@dev\\gra2mol\\VB6\\files\\gen\\VB6.ge3:80:1: (vbFormDefinitionGen+= vbFormDefinition )
            // c:\\@dev\\gra2mol\\VB6\\files\\gen\\VB6.ge3:80:3: vbFormDefinitionGen+= vbFormDefinition
            {
            pushFollow(FOLLOW_vbFormDefinition_in_moduleBody100);
            vbFormDefinitionGen=vbFormDefinition();
            _fsp--;
            if (failed) return retval;
            if (list_vbFormDefinitionGen==null) list_vbFormDefinitionGen=new ArrayList();
            list_vbFormDefinitionGen.add(vbFormDefinitionGen);

            if ( backtracking==0 ) {
              
              		// Create return CST Node
              		Node moduleBodyReturnNode = CSTFactoryImpl.eINSTANCE.createNode();
              		moduleBodyReturnNode.setKind("moduleBody");
              	    // Create a CST Node
              		if(list_vbFormDefinitionGen != null) {
              	        for(Iterator it = list_vbFormDefinitionGen.iterator(); it.hasNext(); )  { 
              	            VB6Parser.vbFormDefinition_return r = (VB6Parser.vbFormDefinition_return) it.next(); 
              	            if(r != null && r.returnNode != null) {
              	            	r.returnNode.setKind("vbFormDefinition");
              	            	moduleBodyReturnNode.getChildren().add(r.returnNode);
              	            } 
              	        }
              	    }
              		// Returns the Node with CST Leaves/Nodes
              		retval.returnNode = moduleBodyReturnNode;
              	
            }

            }

            retval.stop = input.LT(-1);

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
            if ( backtracking>0 ) { memoize(input, 3, moduleBody_StartIndex); }
        }
        return retval;
    }
    // $ANTLR end moduleBody

    public static class vbFormDefinition_return extends ParserRuleReturnScope {
        public Node returnNode;
        public StringTemplate st;
        public Object getTemplate() { return st; }
        public String toString() { return st==null?null:st.toString(); }
    };

    // $ANTLR start vbFormDefinition
    // c:\\@dev\\gra2mol\\VB6\\files\\gen\\VB6.ge3:100:1: vbFormDefinition returns [Node returnNode] : TK_0= 'Begin' TK_1= 'VB.Form' IDGen= ID (vbElementGen+= vbElement )* TK_2= 'End' ;
    public final vbFormDefinition_return vbFormDefinition() throws RecognitionException {
        vbFormDefinition_return retval = new vbFormDefinition_return();
        retval.start = input.LT(1);
        int vbFormDefinition_StartIndex = input.index();
        Token TK_0=null;
        Token TK_1=null;
        Token IDGen=null;
        Token TK_2=null;
        List list_vbElementGen=null;
        RuleReturnScope vbElementGen = null;
        try {
            if ( backtracking>0 && alreadyParsedRule(input, 4) ) { return retval; }
            // c:\\@dev\\gra2mol\\VB6\\files\\gen\\VB6.ge3:101:1: (TK_0= 'Begin' TK_1= 'VB.Form' IDGen= ID (vbElementGen+= vbElement )* TK_2= 'End' )
            // c:\\@dev\\gra2mol\\VB6\\files\\gen\\VB6.ge3:101:3: TK_0= 'Begin' TK_1= 'VB.Form' IDGen= ID (vbElementGen+= vbElement )* TK_2= 'End'
            {
            TK_0=(Token)input.LT(1);
            match(input,11,FOLLOW_11_in_vbFormDefinition121); if (failed) return retval;
            TK_1=(Token)input.LT(1);
            match(input,12,FOLLOW_12_in_vbFormDefinition125); if (failed) return retval;
            IDGen=(Token)input.LT(1);
            match(input,ID,FOLLOW_ID_in_vbFormDefinition129); if (failed) return retval;
            // c:\\@dev\\gra2mol\\VB6\\files\\gen\\VB6.ge3:101:52: (vbElementGen+= vbElement )*
            loop1:
            do {
                int alt1=2;
                int LA1_0 = input.LA(1);

                if ( (LA1_0==11) ) {
                    alt1=1;
                }


                switch (alt1) {
            	case 1 :
            	    // c:\\@dev\\gra2mol\\VB6\\files\\gen\\VB6.ge3:0:0: vbElementGen+= vbElement
            	    {
            	    pushFollow(FOLLOW_vbElement_in_vbFormDefinition133);
            	    vbElementGen=vbElement();
            	    _fsp--;
            	    if (failed) return retval;
            	    if (list_vbElementGen==null) list_vbElementGen=new ArrayList();
            	    list_vbElementGen.add(vbElementGen);


            	    }
            	    break;

            	default :
            	    break loop1;
                }
            } while (true);

            TK_2=(Token)input.LT(1);
            match(input,13,FOLLOW_13_in_vbFormDefinition138); if (failed) return retval;
            if ( backtracking==0 ) {
              
              		// Create return CST Node
              		Node vbFormDefinitionReturnNode = CSTFactoryImpl.eINSTANCE.createNode();
              		vbFormDefinitionReturnNode.setKind("vbFormDefinition");
              	    // Create a Token CST Leaf	
              	    if(TK_0 != null) {
              			Leaf TK_0Leaf = CSTFactoryImpl.eINSTANCE.createLeaf();
              	 		TK_0Leaf.setKind("TOKEN");
              	 		TK_0Leaf.setValue(TK_0.getText());
              			TK_0Leaf.setPos(TK_0.getCharPositionInLine());
              			TK_0Leaf.setLine(TK_0.getLine());
              	 		vbFormDefinitionReturnNode.getChildren().add(TK_0Leaf);
              	 	}
              	    // Create a Token CST Leaf	
              	    if(TK_1 != null) {
              			Leaf TK_1Leaf = CSTFactoryImpl.eINSTANCE.createLeaf();
              	 		TK_1Leaf.setKind("TOKEN");
              	 		TK_1Leaf.setValue(TK_1.getText());
              			TK_1Leaf.setPos(TK_1.getCharPositionInLine());
              			TK_1Leaf.setLine(TK_1.getLine());
              	 		vbFormDefinitionReturnNode.getChildren().add(TK_1Leaf);
              	 	}
              	    // Create a CST Leaf
              		if(IDGen != null) {
              			Leaf IDGenLeaf = CSTFactoryImpl.eINSTANCE.createLeaf();
              			IDGenLeaf.setKind("ID");
              			IDGenLeaf.setValue(IDGen.getText());
              			IDGenLeaf.setPos(IDGen.getCharPositionInLine());
              			IDGenLeaf.setLine(IDGen.getLine());
              			vbFormDefinitionReturnNode.getChildren().add(IDGenLeaf);
              		}
              	    // Create a CST Node
              		if(list_vbElementGen != null) {
              	        for(Iterator it = list_vbElementGen.iterator(); it.hasNext(); )  { 
              	            VB6Parser.vbElement_return r = (VB6Parser.vbElement_return) it.next(); 
              	            if(r != null && r.returnNode != null) {
              	            	r.returnNode.setKind("vbElement");
              	            	vbFormDefinitionReturnNode.getChildren().add(r.returnNode);
              	            } 
              	        }
              	    }
              	    // Create a Token CST Leaf	
              	    if(TK_2 != null) {
              			Leaf TK_2Leaf = CSTFactoryImpl.eINSTANCE.createLeaf();
              	 		TK_2Leaf.setKind("TOKEN");
              	 		TK_2Leaf.setValue(TK_2.getText());
              			TK_2Leaf.setPos(TK_2.getCharPositionInLine());
              			TK_2Leaf.setLine(TK_2.getLine());
              	 		vbFormDefinitionReturnNode.getChildren().add(TK_2Leaf);
              	 	}
              		// Returns the Node with CST Leaves/Nodes
              		retval.returnNode = vbFormDefinitionReturnNode;
              	
            }

            }

            retval.stop = input.LT(-1);

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
            if ( backtracking>0 ) { memoize(input, 4, vbFormDefinition_StartIndex); }
        }
        return retval;
    }
    // $ANTLR end vbFormDefinition

    public static class vbElement_return extends ParserRuleReturnScope {
        public Node returnNode;
        public StringTemplate st;
        public Object getTemplate() { return st; }
        public String toString() { return st==null?null:st.toString(); }
    };

    // $ANTLR start vbElement
    // c:\\@dev\\gra2mol\\VB6\\files\\gen\\VB6.ge3:157:1: vbElement returns [Node returnNode] : (vbFrameDefinitionGen+= vbFrameDefinition | vbCommandButtonDefinitionGen+= vbCommandButtonDefinition | vbTextBoxDefinitionGen+= vbTextBoxDefinition );
    public final vbElement_return vbElement() throws RecognitionException {
        vbElement_return retval = new vbElement_return();
        retval.start = input.LT(1);
        int vbElement_StartIndex = input.index();
        List list_vbFrameDefinitionGen=null;
        List list_vbCommandButtonDefinitionGen=null;
        List list_vbTextBoxDefinitionGen=null;
        RuleReturnScope vbFrameDefinitionGen = null;
        RuleReturnScope vbCommandButtonDefinitionGen = null;
        RuleReturnScope vbTextBoxDefinitionGen = null;
        try {
            if ( backtracking>0 && alreadyParsedRule(input, 5) ) { return retval; }
            // c:\\@dev\\gra2mol\\VB6\\files\\gen\\VB6.ge3:158:1: (vbFrameDefinitionGen+= vbFrameDefinition | vbCommandButtonDefinitionGen+= vbCommandButtonDefinition | vbTextBoxDefinitionGen+= vbTextBoxDefinition )
            int alt2=3;
            int LA2_0 = input.LA(1);

            if ( (LA2_0==11) ) {
                switch ( input.LA(2) ) {
                case 16:
                    {
                    alt2=3;
                    }
                    break;
                case 14:
                    {
                    alt2=1;
                    }
                    break;
                case 15:
                    {
                    alt2=2;
                    }
                    break;
                default:
                    if (backtracking>0) {failed=true; return retval;}
                    NoViableAltException nvae =
                        new NoViableAltException("157:1: vbElement returns [Node returnNode] : (vbFrameDefinitionGen+= vbFrameDefinition | vbCommandButtonDefinitionGen+= vbCommandButtonDefinition | vbTextBoxDefinitionGen+= vbTextBoxDefinition );", 2, 1, input);

                    throw nvae;
                }

            }
            else {
                if (backtracking>0) {failed=true; return retval;}
                NoViableAltException nvae =
                    new NoViableAltException("157:1: vbElement returns [Node returnNode] : (vbFrameDefinitionGen+= vbFrameDefinition | vbCommandButtonDefinitionGen+= vbCommandButtonDefinition | vbTextBoxDefinitionGen+= vbTextBoxDefinition );", 2, 0, input);

                throw nvae;
            }
            switch (alt2) {
                case 1 :
                    // c:\\@dev\\gra2mol\\VB6\\files\\gen\\VB6.ge3:158:3: vbFrameDefinitionGen+= vbFrameDefinition
                    {
                    pushFollow(FOLLOW_vbFrameDefinition_in_vbElement159);
                    vbFrameDefinitionGen=vbFrameDefinition();
                    _fsp--;
                    if (failed) return retval;
                    if (list_vbFrameDefinitionGen==null) list_vbFrameDefinitionGen=new ArrayList();
                    list_vbFrameDefinitionGen.add(vbFrameDefinitionGen);

                    if ( backtracking==0 ) {
                      
                      		// Create return CST Node
                      		Node vbElementReturnNode = CSTFactoryImpl.eINSTANCE.createNode();
                      		vbElementReturnNode.setKind("vbElement");
                      	    // Create a CST Node
                      		if(list_vbFrameDefinitionGen != null) {
                      	        for(Iterator it = list_vbFrameDefinitionGen.iterator(); it.hasNext(); )  { 
                      	            VB6Parser.vbFrameDefinition_return r = (VB6Parser.vbFrameDefinition_return) it.next(); 
                      	            if(r != null && r.returnNode != null) {
                      	            	r.returnNode.setKind("vbFrameDefinition");
                      	            	vbElementReturnNode.getChildren().add(r.returnNode);
                      	            } 
                      	        }
                      	    }
                      		// Returns the Node with CST Leaves/Nodes
                      		retval.returnNode = vbElementReturnNode;
                      	
                    }

                    }
                    break;
                case 2 :
                    // c:\\@dev\\gra2mol\\VB6\\files\\gen\\VB6.ge3:176:4: vbCommandButtonDefinitionGen+= vbCommandButtonDefinition
                    {
                    pushFollow(FOLLOW_vbCommandButtonDefinition_in_vbElement170);
                    vbCommandButtonDefinitionGen=vbCommandButtonDefinition();
                    _fsp--;
                    if (failed) return retval;
                    if (list_vbCommandButtonDefinitionGen==null) list_vbCommandButtonDefinitionGen=new ArrayList();
                    list_vbCommandButtonDefinitionGen.add(vbCommandButtonDefinitionGen);

                    if ( backtracking==0 ) {
                      
                      		// Create return CST Node
                      		Node vbElementReturnNode = CSTFactoryImpl.eINSTANCE.createNode();
                      		vbElementReturnNode.setKind("vbElement");
                      	    // Create a CST Node
                      		if(list_vbCommandButtonDefinitionGen != null) {
                      	        for(Iterator it = list_vbCommandButtonDefinitionGen.iterator(); it.hasNext(); )  { 
                      	            VB6Parser.vbCommandButtonDefinition_return r = (VB6Parser.vbCommandButtonDefinition_return) it.next(); 
                      	            if(r != null && r.returnNode != null) {
                      	            	r.returnNode.setKind("vbCommandButtonDefinition");
                      	            	vbElementReturnNode.getChildren().add(r.returnNode);
                      	            } 
                      	        }
                      	    }
                      		// Returns the Node with CST Leaves/Nodes
                      		retval.returnNode = vbElementReturnNode;
                      	
                    }

                    }
                    break;
                case 3 :
                    // c:\\@dev\\gra2mol\\VB6\\files\\gen\\VB6.ge3:194:4: vbTextBoxDefinitionGen+= vbTextBoxDefinition
                    {
                    pushFollow(FOLLOW_vbTextBoxDefinition_in_vbElement181);
                    vbTextBoxDefinitionGen=vbTextBoxDefinition();
                    _fsp--;
                    if (failed) return retval;
                    if (list_vbTextBoxDefinitionGen==null) list_vbTextBoxDefinitionGen=new ArrayList();
                    list_vbTextBoxDefinitionGen.add(vbTextBoxDefinitionGen);

                    if ( backtracking==0 ) {
                      
                      		// Create return CST Node
                      		Node vbElementReturnNode = CSTFactoryImpl.eINSTANCE.createNode();
                      		vbElementReturnNode.setKind("vbElement");
                      	    // Create a CST Node
                      		if(list_vbTextBoxDefinitionGen != null) {
                      	        for(Iterator it = list_vbTextBoxDefinitionGen.iterator(); it.hasNext(); )  { 
                      	            VB6Parser.vbTextBoxDefinition_return r = (VB6Parser.vbTextBoxDefinition_return) it.next(); 
                      	            if(r != null && r.returnNode != null) {
                      	            	r.returnNode.setKind("vbTextBoxDefinition");
                      	            	vbElementReturnNode.getChildren().add(r.returnNode);
                      	            } 
                      	        }
                      	    }
                      		// Returns the Node with CST Leaves/Nodes
                      		retval.returnNode = vbElementReturnNode;
                      	
                    }

                    }
                    break;

            }
            retval.stop = input.LT(-1);

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
            if ( backtracking>0 ) { memoize(input, 5, vbElement_StartIndex); }
        }
        return retval;
    }
    // $ANTLR end vbElement

    public static class vbFrameDefinition_return extends ParserRuleReturnScope {
        public Node returnNode;
        public StringTemplate st;
        public Object getTemplate() { return st; }
        public String toString() { return st==null?null:st.toString(); }
    };

    // $ANTLR start vbFrameDefinition
    // c:\\@dev\\gra2mol\\VB6\\files\\gen\\VB6.ge3:214:1: vbFrameDefinition returns [Node returnNode] : TK_0= 'Begin' TK_1= 'VB.Frame' IDGen= ID (vbElementGen+= vbElement )* TK_2= 'End' ;
    public final vbFrameDefinition_return vbFrameDefinition() throws RecognitionException {
        vbFrameDefinition_return retval = new vbFrameDefinition_return();
        retval.start = input.LT(1);
        int vbFrameDefinition_StartIndex = input.index();
        Token TK_0=null;
        Token TK_1=null;
        Token IDGen=null;
        Token TK_2=null;
        List list_vbElementGen=null;
        RuleReturnScope vbElementGen = null;
        try {
            if ( backtracking>0 && alreadyParsedRule(input, 6) ) { return retval; }
            // c:\\@dev\\gra2mol\\VB6\\files\\gen\\VB6.ge3:215:1: (TK_0= 'Begin' TK_1= 'VB.Frame' IDGen= ID (vbElementGen+= vbElement )* TK_2= 'End' )
            // c:\\@dev\\gra2mol\\VB6\\files\\gen\\VB6.ge3:215:3: TK_0= 'Begin' TK_1= 'VB.Frame' IDGen= ID (vbElementGen+= vbElement )* TK_2= 'End'
            {
            TK_0=(Token)input.LT(1);
            match(input,11,FOLLOW_11_in_vbFrameDefinition202); if (failed) return retval;
            TK_1=(Token)input.LT(1);
            match(input,14,FOLLOW_14_in_vbFrameDefinition206); if (failed) return retval;
            IDGen=(Token)input.LT(1);
            match(input,ID,FOLLOW_ID_in_vbFrameDefinition210); if (failed) return retval;
            // c:\\@dev\\gra2mol\\VB6\\files\\gen\\VB6.ge3:215:53: (vbElementGen+= vbElement )*
            loop3:
            do {
                int alt3=2;
                int LA3_0 = input.LA(1);

                if ( (LA3_0==11) ) {
                    alt3=1;
                }


                switch (alt3) {
            	case 1 :
            	    // c:\\@dev\\gra2mol\\VB6\\files\\gen\\VB6.ge3:0:0: vbElementGen+= vbElement
            	    {
            	    pushFollow(FOLLOW_vbElement_in_vbFrameDefinition214);
            	    vbElementGen=vbElement();
            	    _fsp--;
            	    if (failed) return retval;
            	    if (list_vbElementGen==null) list_vbElementGen=new ArrayList();
            	    list_vbElementGen.add(vbElementGen);


            	    }
            	    break;

            	default :
            	    break loop3;
                }
            } while (true);

            TK_2=(Token)input.LT(1);
            match(input,13,FOLLOW_13_in_vbFrameDefinition219); if (failed) return retval;
            if ( backtracking==0 ) {
              
              		// Create return CST Node
              		Node vbFrameDefinitionReturnNode = CSTFactoryImpl.eINSTANCE.createNode();
              		vbFrameDefinitionReturnNode.setKind("vbFrameDefinition");
              	    // Create a Token CST Leaf	
              	    if(TK_0 != null) {
              			Leaf TK_0Leaf = CSTFactoryImpl.eINSTANCE.createLeaf();
              	 		TK_0Leaf.setKind("TOKEN");
              	 		TK_0Leaf.setValue(TK_0.getText());
              			TK_0Leaf.setPos(TK_0.getCharPositionInLine());
              			TK_0Leaf.setLine(TK_0.getLine());
              	 		vbFrameDefinitionReturnNode.getChildren().add(TK_0Leaf);
              	 	}
              	    // Create a Token CST Leaf	
              	    if(TK_1 != null) {
              			Leaf TK_1Leaf = CSTFactoryImpl.eINSTANCE.createLeaf();
              	 		TK_1Leaf.setKind("TOKEN");
              	 		TK_1Leaf.setValue(TK_1.getText());
              			TK_1Leaf.setPos(TK_1.getCharPositionInLine());
              			TK_1Leaf.setLine(TK_1.getLine());
              	 		vbFrameDefinitionReturnNode.getChildren().add(TK_1Leaf);
              	 	}
              	    // Create a CST Leaf
              		if(IDGen != null) {
              			Leaf IDGenLeaf = CSTFactoryImpl.eINSTANCE.createLeaf();
              			IDGenLeaf.setKind("ID");
              			IDGenLeaf.setValue(IDGen.getText());
              			IDGenLeaf.setPos(IDGen.getCharPositionInLine());
              			IDGenLeaf.setLine(IDGen.getLine());
              			vbFrameDefinitionReturnNode.getChildren().add(IDGenLeaf);
              		}
              	    // Create a CST Node
              		if(list_vbElementGen != null) {
              	        for(Iterator it = list_vbElementGen.iterator(); it.hasNext(); )  { 
              	            VB6Parser.vbElement_return r = (VB6Parser.vbElement_return) it.next(); 
              	            if(r != null && r.returnNode != null) {
              	            	r.returnNode.setKind("vbElement");
              	            	vbFrameDefinitionReturnNode.getChildren().add(r.returnNode);
              	            } 
              	        }
              	    }
              	    // Create a Token CST Leaf	
              	    if(TK_2 != null) {
              			Leaf TK_2Leaf = CSTFactoryImpl.eINSTANCE.createLeaf();
              	 		TK_2Leaf.setKind("TOKEN");
              	 		TK_2Leaf.setValue(TK_2.getText());
              			TK_2Leaf.setPos(TK_2.getCharPositionInLine());
              			TK_2Leaf.setLine(TK_2.getLine());
              	 		vbFrameDefinitionReturnNode.getChildren().add(TK_2Leaf);
              	 	}
              		// Returns the Node with CST Leaves/Nodes
              		retval.returnNode = vbFrameDefinitionReturnNode;
              	
            }

            }

            retval.stop = input.LT(-1);

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
            if ( backtracking>0 ) { memoize(input, 6, vbFrameDefinition_StartIndex); }
        }
        return retval;
    }
    // $ANTLR end vbFrameDefinition

    public static class vbCommandButtonDefinition_return extends ParserRuleReturnScope {
        public Node returnNode;
        public StringTemplate st;
        public Object getTemplate() { return st; }
        public String toString() { return st==null?null:st.toString(); }
    };

    // $ANTLR start vbCommandButtonDefinition
    // c:\\@dev\\gra2mol\\VB6\\files\\gen\\VB6.ge3:271:1: vbCommandButtonDefinition returns [Node returnNode] : TK_0= 'Begin' TK_1= 'VB.CommandButton' IDGen= ID TK_2= 'End' ;
    public final vbCommandButtonDefinition_return vbCommandButtonDefinition() throws RecognitionException {
        vbCommandButtonDefinition_return retval = new vbCommandButtonDefinition_return();
        retval.start = input.LT(1);
        int vbCommandButtonDefinition_StartIndex = input.index();
        Token TK_0=null;
        Token TK_1=null;
        Token IDGen=null;
        Token TK_2=null;

        try {
            if ( backtracking>0 && alreadyParsedRule(input, 7) ) { return retval; }
            // c:\\@dev\\gra2mol\\VB6\\files\\gen\\VB6.ge3:272:1: (TK_0= 'Begin' TK_1= 'VB.CommandButton' IDGen= ID TK_2= 'End' )
            // c:\\@dev\\gra2mol\\VB6\\files\\gen\\VB6.ge3:272:3: TK_0= 'Begin' TK_1= 'VB.CommandButton' IDGen= ID TK_2= 'End'
            {
            TK_0=(Token)input.LT(1);
            match(input,11,FOLLOW_11_in_vbCommandButtonDefinition240); if (failed) return retval;
            TK_1=(Token)input.LT(1);
            match(input,15,FOLLOW_15_in_vbCommandButtonDefinition244); if (failed) return retval;
            IDGen=(Token)input.LT(1);
            match(input,ID,FOLLOW_ID_in_vbCommandButtonDefinition248); if (failed) return retval;
            TK_2=(Token)input.LT(1);
            match(input,13,FOLLOW_13_in_vbCommandButtonDefinition252); if (failed) return retval;
            if ( backtracking==0 ) {
              
              		// Create return CST Node
              		Node vbCommandButtonDefinitionReturnNode = CSTFactoryImpl.eINSTANCE.createNode();
              		vbCommandButtonDefinitionReturnNode.setKind("vbCommandButtonDefinition");
              	    // Create a Token CST Leaf	
              	    if(TK_0 != null) {
              			Leaf TK_0Leaf = CSTFactoryImpl.eINSTANCE.createLeaf();
              	 		TK_0Leaf.setKind("TOKEN");
              	 		TK_0Leaf.setValue(TK_0.getText());
              			TK_0Leaf.setPos(TK_0.getCharPositionInLine());
              			TK_0Leaf.setLine(TK_0.getLine());
              	 		vbCommandButtonDefinitionReturnNode.getChildren().add(TK_0Leaf);
              	 	}
              	    // Create a Token CST Leaf	
              	    if(TK_1 != null) {
              			Leaf TK_1Leaf = CSTFactoryImpl.eINSTANCE.createLeaf();
              	 		TK_1Leaf.setKind("TOKEN");
              	 		TK_1Leaf.setValue(TK_1.getText());
              			TK_1Leaf.setPos(TK_1.getCharPositionInLine());
              			TK_1Leaf.setLine(TK_1.getLine());
              	 		vbCommandButtonDefinitionReturnNode.getChildren().add(TK_1Leaf);
              	 	}
              	    // Create a CST Leaf
              		if(IDGen != null) {
              			Leaf IDGenLeaf = CSTFactoryImpl.eINSTANCE.createLeaf();
              			IDGenLeaf.setKind("ID");
              			IDGenLeaf.setValue(IDGen.getText());
              			IDGenLeaf.setPos(IDGen.getCharPositionInLine());
              			IDGenLeaf.setLine(IDGen.getLine());
              			vbCommandButtonDefinitionReturnNode.getChildren().add(IDGenLeaf);
              		}
              	    // Create a Token CST Leaf	
              	    if(TK_2 != null) {
              			Leaf TK_2Leaf = CSTFactoryImpl.eINSTANCE.createLeaf();
              	 		TK_2Leaf.setKind("TOKEN");
              	 		TK_2Leaf.setValue(TK_2.getText());
              			TK_2Leaf.setPos(TK_2.getCharPositionInLine());
              			TK_2Leaf.setLine(TK_2.getLine());
              	 		vbCommandButtonDefinitionReturnNode.getChildren().add(TK_2Leaf);
              	 	}
              		// Returns the Node with CST Leaves/Nodes
              		retval.returnNode = vbCommandButtonDefinitionReturnNode;
              	
            }

            }

            retval.stop = input.LT(-1);

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
            if ( backtracking>0 ) { memoize(input, 7, vbCommandButtonDefinition_StartIndex); }
        }
        return retval;
    }
    // $ANTLR end vbCommandButtonDefinition

    public static class vbTextBoxDefinition_return extends ParserRuleReturnScope {
        public Node returnNode;
        public StringTemplate st;
        public Object getTemplate() { return st; }
        public String toString() { return st==null?null:st.toString(); }
    };

    // $ANTLR start vbTextBoxDefinition
    // c:\\@dev\\gra2mol\\VB6\\files\\gen\\VB6.ge3:318:1: vbTextBoxDefinition returns [Node returnNode] : TK_0= 'Begin' TK_1= 'VB.TextBox' IDGen= ID TK_2= 'End' ;
    public final vbTextBoxDefinition_return vbTextBoxDefinition() throws RecognitionException {
        vbTextBoxDefinition_return retval = new vbTextBoxDefinition_return();
        retval.start = input.LT(1);
        int vbTextBoxDefinition_StartIndex = input.index();
        Token TK_0=null;
        Token TK_1=null;
        Token IDGen=null;
        Token TK_2=null;

        try {
            if ( backtracking>0 && alreadyParsedRule(input, 8) ) { return retval; }
            // c:\\@dev\\gra2mol\\VB6\\files\\gen\\VB6.ge3:319:1: (TK_0= 'Begin' TK_1= 'VB.TextBox' IDGen= ID TK_2= 'End' )
            // c:\\@dev\\gra2mol\\VB6\\files\\gen\\VB6.ge3:319:3: TK_0= 'Begin' TK_1= 'VB.TextBox' IDGen= ID TK_2= 'End'
            {
            TK_0=(Token)input.LT(1);
            match(input,11,FOLLOW_11_in_vbTextBoxDefinition273); if (failed) return retval;
            TK_1=(Token)input.LT(1);
            match(input,16,FOLLOW_16_in_vbTextBoxDefinition277); if (failed) return retval;
            IDGen=(Token)input.LT(1);
            match(input,ID,FOLLOW_ID_in_vbTextBoxDefinition281); if (failed) return retval;
            TK_2=(Token)input.LT(1);
            match(input,13,FOLLOW_13_in_vbTextBoxDefinition285); if (failed) return retval;
            if ( backtracking==0 ) {
              
              		// Create return CST Node
              		Node vbTextBoxDefinitionReturnNode = CSTFactoryImpl.eINSTANCE.createNode();
              		vbTextBoxDefinitionReturnNode.setKind("vbTextBoxDefinition");
              	    // Create a Token CST Leaf	
              	    if(TK_0 != null) {
              			Leaf TK_0Leaf = CSTFactoryImpl.eINSTANCE.createLeaf();
              	 		TK_0Leaf.setKind("TOKEN");
              	 		TK_0Leaf.setValue(TK_0.getText());
              			TK_0Leaf.setPos(TK_0.getCharPositionInLine());
              			TK_0Leaf.setLine(TK_0.getLine());
              	 		vbTextBoxDefinitionReturnNode.getChildren().add(TK_0Leaf);
              	 	}
              	    // Create a Token CST Leaf	
              	    if(TK_1 != null) {
              			Leaf TK_1Leaf = CSTFactoryImpl.eINSTANCE.createLeaf();
              	 		TK_1Leaf.setKind("TOKEN");
              	 		TK_1Leaf.setValue(TK_1.getText());
              			TK_1Leaf.setPos(TK_1.getCharPositionInLine());
              			TK_1Leaf.setLine(TK_1.getLine());
              	 		vbTextBoxDefinitionReturnNode.getChildren().add(TK_1Leaf);
              	 	}
              	    // Create a CST Leaf
              		if(IDGen != null) {
              			Leaf IDGenLeaf = CSTFactoryImpl.eINSTANCE.createLeaf();
              			IDGenLeaf.setKind("ID");
              			IDGenLeaf.setValue(IDGen.getText());
              			IDGenLeaf.setPos(IDGen.getCharPositionInLine());
              			IDGenLeaf.setLine(IDGen.getLine());
              			vbTextBoxDefinitionReturnNode.getChildren().add(IDGenLeaf);
              		}
              	    // Create a Token CST Leaf	
              	    if(TK_2 != null) {
              			Leaf TK_2Leaf = CSTFactoryImpl.eINSTANCE.createLeaf();
              	 		TK_2Leaf.setKind("TOKEN");
              	 		TK_2Leaf.setValue(TK_2.getText());
              			TK_2Leaf.setPos(TK_2.getCharPositionInLine());
              			TK_2Leaf.setLine(TK_2.getLine());
              	 		vbTextBoxDefinitionReturnNode.getChildren().add(TK_2Leaf);
              	 	}
              		// Returns the Node with CST Leaves/Nodes
              		retval.returnNode = vbTextBoxDefinitionReturnNode;
              	
            }

            }

            retval.stop = input.LT(-1);

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
            if ( backtracking>0 ) { memoize(input, 8, vbTextBoxDefinition_StartIndex); }
        }
        return retval;
    }
    // $ANTLR end vbTextBoxDefinition


 

    public static final BitSet FOLLOW_moduleHeader_in_module50 = new BitSet(new long[]{0x0000000000000800L});
    public static final BitSet FOLLOW_moduleBody_in_module54 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_10_in_moduleHeader75 = new BitSet(new long[]{0x0000000000000010L});
    public static final BitSet FOLLOW_FloatingPointLiteral_in_moduleHeader79 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_vbFormDefinition_in_moduleBody100 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_11_in_vbFormDefinition121 = new BitSet(new long[]{0x0000000000001000L});
    public static final BitSet FOLLOW_12_in_vbFormDefinition125 = new BitSet(new long[]{0x0000000000000020L});
    public static final BitSet FOLLOW_ID_in_vbFormDefinition129 = new BitSet(new long[]{0x0000000000002800L});
    public static final BitSet FOLLOW_vbElement_in_vbFormDefinition133 = new BitSet(new long[]{0x0000000000002800L});
    public static final BitSet FOLLOW_13_in_vbFormDefinition138 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_vbFrameDefinition_in_vbElement159 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_vbCommandButtonDefinition_in_vbElement170 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_vbTextBoxDefinition_in_vbElement181 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_11_in_vbFrameDefinition202 = new BitSet(new long[]{0x0000000000004000L});
    public static final BitSet FOLLOW_14_in_vbFrameDefinition206 = new BitSet(new long[]{0x0000000000000020L});
    public static final BitSet FOLLOW_ID_in_vbFrameDefinition210 = new BitSet(new long[]{0x0000000000002800L});
    public static final BitSet FOLLOW_vbElement_in_vbFrameDefinition214 = new BitSet(new long[]{0x0000000000002800L});
    public static final BitSet FOLLOW_13_in_vbFrameDefinition219 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_11_in_vbCommandButtonDefinition240 = new BitSet(new long[]{0x0000000000008000L});
    public static final BitSet FOLLOW_15_in_vbCommandButtonDefinition244 = new BitSet(new long[]{0x0000000000000020L});
    public static final BitSet FOLLOW_ID_in_vbCommandButtonDefinition248 = new BitSet(new long[]{0x0000000000002000L});
    public static final BitSet FOLLOW_13_in_vbCommandButtonDefinition252 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_11_in_vbTextBoxDefinition273 = new BitSet(new long[]{0x0000000000010000L});
    public static final BitSet FOLLOW_16_in_vbTextBoxDefinition277 = new BitSet(new long[]{0x0000000000000020L});
    public static final BitSet FOLLOW_ID_in_vbTextBoxDefinition281 = new BitSet(new long[]{0x0000000000002000L});
    public static final BitSet FOLLOW_13_in_vbTextBoxDefinition285 = new BitSet(new long[]{0x0000000000000002L});

}